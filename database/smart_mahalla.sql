create table `tuman`
(
    id   int auto_increment
        primary key,
    name varchar(255) null
);

create table `tumanbolim`
(
    id        int auto_increment
        primary key,
    name      varchar(100) null,
    full_name varchar(255) null,
    position  varchar(100) null,
    tel       varchar(30)  null,
    tuman_id  int          null,
    constraint tumanBolim_tuman_id_fk
        foreign key (tuman_id) references tuman (id)
);

create table `mahalla`
(
    id        int auto_increment
        primary key,
    name      varchar(100) null,
    full_name varchar(255) null,
    tel       varchar(30)  null,
    tuman_id  int          null,
    constraint mahalla_tuman_id_fk
        foreign key (tuman_id) references tuman (id)
);

create table `user`
(
    id         int auto_increment
        primary key,
    chat_id    int          null,
    page       varchar(100) null,
    created_at datetime     null,
    username   varchar(255) null,
    tuman_id   int          null,
    constraint user_chat_id_uindex
        unique (chat_id)
);

