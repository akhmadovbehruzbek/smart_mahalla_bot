<?php

use Parmonov98\Dotenv\Dotenv;

$env = new DotEnv();

/**
 * Path name
 */

const ERROR_PATH = 'error_log';
const IMAGE_PATH = 'http://smart-mahalla-bot.local/images/';
const DOCUMENT_PATH = 'http://smart-mahalla-bot.local/downloads/';

/*
 * Db configuration
 * */
define("HOST", $env->get("HOST"));
define("USER", $env->get("USER"));
define("PASS", $env->get("PASS"));
define("DB_NAME", $env->get("DB_NAME"));


/**
 *  Bot configuration
 */

define("WEBHOOK_URL", $env->get("WEBHOOK_URL"));
define("ADMIN_CHAT_ID_1", $env->get("ADMIN_CHAT_ID_1"));
define("ADMIN_CHAT_ID_2", $env->get("ADMIN_CHAT_ID_2"));
define("BOT_TOKEN", $env->get("BOT_TOKEN"));

/**
 *  Message
 */

const WELCOME_USER = "Assalomu alaykum hurmatli @JahongirXakimovich !  Yoshlar ishlari agentligi Surxondaryo viloyati boshqarmasi Mahalladagi yoshlar yetakchilari bilan ishlash bo'limi botiga xush kelibsiz!";
const BOT_NAME = '🏘 Surxondaryo Yetakchilar Bot';