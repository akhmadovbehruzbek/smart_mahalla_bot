<?php

namespace BotKeyboard;

use Illuminate\Support\Traits\EnumeratesValues;
use Telegram\Bot\Keyboard\Keyboard;

class BotKeyboard
{
    public static function inlineKeyboard($data, $dataCount): EnumeratesValues|Keyboard
    {
        $keyboard = Keyboard::make()->inline();

        if ($dataCount % 2 === 0) {
            for ($i = 0; $i < $dataCount; $i += 2) {
                $keyboard->row(
                    Keyboard::inlineButton(['text' => $data[$i]['name'], 'callback_data' => 'tuman_' . $data[$i]['id']]),
                    Keyboard::inlineButton(['text' => $data[$i + 1]['name'], 'callback_data' => 'tuman_' . $data[$i + 1]['id']])
                );
            }
        } else {
            for ($i = 0; $i < $dataCount - 1; $i += 2) {
                $keyboard->row(
                    Keyboard::inlineButton(['text' => $data[$i]['name'], 'callback_data' => 'tuman_' . $data[$i]['id']]),
                    Keyboard::inlineButton(['text' => $data[$i + 1]['name'], 'callback_data' => 'tuman_' . $data[$i + 1]['id']])
                );
            }
            $keyboard->row(
                Keyboard::inlineButton(['text' => end($data)['name'], 'callback_data' => 'tuman_' . end($data)['id']]),
            );
        }


        return $keyboard;
    }

    public static function keyboard($data, $dataCount): EnumeratesValues|Keyboard
    {
        $keyboard = Keyboard::make();

        if ($dataCount % 2 === 0) {
            for ($i = 0; $i < $dataCount; $i += 2) {
                $keyboard->row(
                    Keyboard::inlineButton(['text' => $data[$i]['name']]),
                    Keyboard::inlineButton(['text' => $data[$i + 1]['name']])
                );
            }
        } else {
            for ($i = 0; $i < $dataCount - 1; $i += 2) {
                $keyboard->row(
                    Keyboard::inlineButton(['text' => $data[$i]['name']]),
                    Keyboard::inlineButton(['text' => $data[$i + 1]['name']])
                );
            }
            $keyboard->row(
                Keyboard::inlineButton(['text' => end($data)['name']]),
            );
        }
        $keyboard->row(
            Keyboard::button(['text' => '🔙Orqaga']),
        );

        return $keyboard->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function startKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()->row(
            Keyboard::button(['text' => '/start'])
        )->setResizeKeyboard(true)->setOneTimeKeyboard(true);
    }

    public static function test()
    {
        return Keyboard::make()->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'test', 'callback_data' => 'tuman']),
            );
    }

    public static function agentMahallaKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()->row(
            Keyboard::button(['text' => "Bo'lim xodimlari"]),
            Keyboard::button(['text' => "Mahalla yetakchilari"])
        )->row(
            Keyboard::button(['text' => '🔙Orqaga'])
        )->setOneTimeKeyboard(false)->setResizeKeyboard(true);
    }

    public static function homeKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()->row(
            Keyboard::button(['text' => 'Hududlar🏛'])
        )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function homeAdminKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()->row(
            Keyboard::button(['text' => 'Tuman']),
        )->row(
            Keyboard::button(['text' => 'Tuman Bo\'limlari']),
            Keyboard::button(['text' => 'Mahalla']),
        )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function backAdminKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()->row(
            Keyboard::button(['text' => '🔙Orqaga'])
        )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function adminTumanKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()
            ->row(
                Keyboard::button(['text' => 'Barcha tumanlar']),
                Keyboard::button(['text' => '🔙Orqaga'])
            )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function adminMahallaKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()
            ->row(
                Keyboard::button(['text' => 'Mahallalarni yangilash'])
            )
            ->row(
                Keyboard::button(['text' => 'Barcha mahallalar']),
                Keyboard::button(['text' => '🔙Orqaga'])
            )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

    public static function adminTumanBolimKeyboard(): EnumeratesValues|Keyboard
    {
        return Keyboard::make()
            ->row(
                Keyboard::button(['text' => "Tuman bo'limlarini yangilash"])
            )
            ->row(
                Keyboard::button(['text' => 'Barcha tuman bo\'limlari']),
                Keyboard::button(['text' => '🔙Orqaga'])
            )->setResizeKeyboard(true)->setOneTimeKeyboard(false);
    }

}