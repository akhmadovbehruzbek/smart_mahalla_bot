<?php

namespace class\Db;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use mysql_xdevapi\Exception;
use RedBeanPHP\R;
use RedBeanPHP\RedException\SQL;

require_once 'connect.php';

class Db
{

    private string $host = HOST;
    private string $user = USER;
    private string $pass = PASS;
    private string $db_name = DB_NAME;

//    public function __construct()
//    {
//        try {
//            R::setup("mysql:host=$this->host;dbname=$this->db_name",
//                $this->user, $this->pass);
//        } catch (Exception $e) {
//            echo $e->getMessage();
//        }
//
//    }

    public function __destruct()
    {
        R::close();
    }

    public function setPage($page, $chat_id): array|int|null
    {
        return R::exec(/** @lang text */ 'UPDATE `user` SET page=? WHERE chat_id = ?', [$page, $chat_id]);
    }


    /** Find user */
    public function findUser($chat_id): ?\RedBeanPHP\OODBBean
    {
        return R::findOne('user', ' chat_id = ?', [
            $chat_id
        ]);
    }

    /** Add New user
     * @throws SQL
     */
    public function addUser($chat_id, $page, $username): int|string
    {
        $user = R::dispense('user');
        $user->chat_id = $chat_id;
        $user->page = $page;
        $user->username = $username;
        $user->created_at = R::isoDateTime();
        return R::store($user);
    }

    public function getPage($chat_id): ?array
    {
        return R::getRow(/** @lang text */ 'SELECT `page` FROM `user` WHERE chat_id = ?', [$chat_id]);
    }

    public function getTumanAll(): array
    {
        return R::getAll(/** @lang text */ 'select * from `tuman` ORDER BY name ASC');
    }

    public function getTumanBolimAllAdmin(): array
    {
        return R::getAll(/** @lang text */ 'select * from `tumanbolim` ORDER BY name ASC');
    }

    public function getMahallaAllAdmin(): array
    {
        return R::getAll(/** @lang text */ 'select * from `mahalla`');
    }

    public function trashTable($table): bool
    {
        return R::wipe($table);
    }

    public function count($table): int
    {
        return R::count($table);
    }

    public function count_mahalla($table, $id): int
    {
        return R::count($table, 'tuman_id = ?', [$id]);
    }

    public function getTumanIdByUser($chat_id): ?array
    {
        return R::getRow(/** @lang text */ 'SELECT `tuman_id` FROM `user` WHERE chat_id = ?', [$chat_id]);
    }

    public function getAgentlikAll($tuman_id): ?array
    {
        return R::getAll(/** @lang text */ 'SELECT `id`, `name` FROM `tumanbolim` WHERE tuman_id = ?', [$tuman_id]);
    }

    public function getMahallaAll($tuman_id): ?array
    {
        return R::getAll(/** @lang text */ 'SELECT `id`, `name` FROM `mahalla` WHERE tuman_id = ?', [$tuman_id]);
    }


    public function getMahalla($data): ?array
    {
        return R::getRow(/** @lang text */ 'SELECT * FROM `mahalla`  WHERE  name = ?', [$data]);
    }

    public function getAgentlik($data): ?array
    {
        return R::getRow(/** @lang text */ 'SELECT * FROM `tumanbolim` WHERE  name = ?', [$data]);
    }


    public function getTumaniId($tuman): ?array
    {
        return R::getRow(/** @lang text */ 'SELECT `id` FROM `tuman` WHERE name = ?', [$tuman]);
    }

    public function saveTumanId($tuman_id, $user_chat_id): array|int|null
    {
        return R::exec(/** @lang text */ 'UPDATE `user` SET tuman_id = ? WHERE chat_id = ?', [$tuman_id, $user_chat_id]);
    }


    /**
     * @param $data
     * @return int|string
     */
    public function addNewTuman($data): int|string
    {
        $allData = [];
        foreach ($data as $d) {
            $singlData = R::dispense('tuman');
            $singlData->name = $d[0];
            $allData[] = $singlData;
        }
        try {
            R::exec('SET FOREIGN_KEY_CHECKS = 0;');
            R::begin();
            R::storeAll($allData);
            R::commit();
            R::exec('SET FOREIGN_KEY_CHECKS = 1;');
            return true;
        } catch (\Exception $e) {
            print_r($e);
            exit();
        }
    }

    /**
     * @param $data
     * @return int|string
     */
    public function addNewMahalla($data): int|string
    {
        $allData = [];
        foreach ($data as $d) {

            $singlData = R::dispense('mahalla');
            $singlData->name = $d[0];
            $singlData->full_name = $d[1];
            $singlData->tel = $d[2];
            $singlData->tuman_id = $d[3];

            $allData[] = $singlData;
        }
        try {
            R::exec('SET FOREIGN_KEY_CHECKS = 0;');
            R::begin();
            R::storeAll($allData);
            R::commit();
            R::exec('SET FOREIGN_KEY_CHECKS = 1;');
            return true;
        } catch (\Exception $e) {
            print_r($e);
            exit();
        }
    }

    /**
     * @param $data
     * @return int|string
     */
    public function addNewTumanBolim($data): int|string
    {
        $allData = [];
        foreach ($data as $d) {

            $singlData = R::dispense('tumanbolim');
            $singlData->name = $d[0];
            $singlData->full_name = $d[1];
            $singlData->position = $d[2];
            $singlData->tel = $d[3];
            $singlData->tuman_id = $d[4];

            $allData[] = $singlData;
        }
        try {
            R::exec('SET FOREIGN_KEY_CHECKS = 0;');
            R::begin();
            R::storeAll($allData);
            R::commit();
            R::exec('SET FOREIGN_KEY_CHECKS = 1;');
            return true;
        } catch (\Exception $e) {
            print_r($e);
            exit();
        }

    }


}