<?php

namespace class\Core;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\FileUpload\InputFile;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config.php';

class Core
{
    private static mixed $bot_token = BOT_TOKEN;

    /**
     * @throws TelegramSDKException
     */
    public static function Api(): Api
    {
        return new Api(self::$bot_token);
    }

    /**
     * @throws TelegramSDKException
     */
    public static function send($chat_id, $text, $keyboard = null, string $parse_mode = 'html'): \Telegram\Bot\Objects\Message
    {
        return self::Api()->sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
            'parse_mode' => $parse_mode
        ]);
    }

    /**
     * @throws TelegramSDKException
     */
    public static function sendPhoto($chat_id, string $photo, $caption = null, $keyboard = null, string $parse_mode = 'html'): \Telegram\Bot\Objects\Message
    {
        $path_arr = explode('/', $photo);
//        $path= preg_replace('/\W\w+\s*(\W*)$/', '$1', $photo);
        return self::Api()->sendPhoto([
            'chat_id' => $chat_id,
            'photo' => InputFile::create($photo, end($path_arr)),
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'reply_markup' => $keyboard,

        ]);
    }

    /**
     * @throws TelegramSDKException
     */
    public static function sendDocument($chat_id, string $document, $caption = null, $keyboard = null, string $parse_mode = 'html'): \Telegram\Bot\Objects\Message
    {
        $path_arr = explode('/', $document);
//        $path= preg_replace('/\W\w+\s*(\W*)$/', '$1', $photo);
        return self::Api()->sendDocument([
            'chat_id' => $chat_id,
            'document' => InputFile::create($document, end($path_arr)),
            'caption' => $caption,
            'parse_mode' => $parse_mode,
            'reply_markup' => $keyboard,
        ]);
    }

    /**
     * @throws TelegramSDKException
     */
    public static function edit($chat_id, $message_id, $text, $keyboard = null, string $parse_mode = 'html'): \Telegram\Bot\Objects\Message|bool
    {
        return self::Api()->editMessageText([
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $text,
            'reply_markup' => $keyboard,
            'parse_mode' => $parse_mode
        ]);
    }

    /**
     * @throws TelegramSDKException
     */
    public static function delete($chat_id, $message_id): \Telegram\Bot\Objects\Message|bool
    {
        return self::Api()->deleteMessage([
            'chat_id' => $chat_id,
            'message_id' => $message_id
        ]);
    }
    /************************************** *******************************/

    /**
     * @throws TelegramSDKException
     */
    public static function result($method)
    {
        return self::Api()->getWebhookUpdate()[$method];
    }

    /**
     * @throws TelegramSDKException
     */
    public static function chatId()
    {
        return self::result('message')['chat']['id'];
    }

    /**
     * @throws TelegramSDKException
     */
    public static function messageId()
    {
        return self::result('message')['message_id'];
    }

    /**
     * @throws TelegramSDKException
     */
    public static function userName(): string
    {
        return self::result('message')['chat']['username'] == '' ? self::result('message')['chat']['first_name'] : '@' . self::result('message')['chat']['username'];
    }

    /**
     * @throws TelegramSDKException
     */
    public static function text(): string
    {
        $text = isset(self::result('message')['text']) ? self::result('message')['text'] : '';
        return trim($text);
    }

    /*******  File  ****
     * @throws TelegramSDKException
     */
    public static function file($method)
    {
        return Core::result('message')['document'][$method];
    }

    /**
     * @throws TelegramSDKException
     */
    public static function file_id()
    {
        return self::file('file_id');
    }

    /**
     * @throws TelegramSDKException
     */
    public static function file_name()
    {
        return self::file('file_name');
    }

    /**
     * @throws TelegramSDKException
     */
    public static function downloadFile(): bool|string
    {
        $response = self::Api()->getFile(['file_id' => self::file_id()]);
        return file_get_contents("https://api.telegram.org/file/bot" . BOT_TOKEN . "/" . $response->filePath . "?file_id=" . self::file_id());
    }

    /**
     * @throws TelegramSDKException
     */
    public static function validateFile(): bool
    {
        $allowed_extension = ['xls', 'xlsx'];
        $file_array = explode(".", self::file_name());
        $file_extension = end($file_array);
        return in_array($file_extension, $allowed_extension);
    }

    public static function putFile($path, $file): bool|int
    {
        return file_put_contents("downloads/" . $path . "/" . $path . '.xlsx', $file);
    }


    /**
     * @throws TelegramSDKException
     */
    public static function setFile($name): bool|int
    {

        if (self::validateFile()) {
            return self::putFile($name, self::downloadFile());
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public static function getDataArray($dirname): array
    {
        $file_type = IOFactory::identify('downloads/' . $dirname . '/' . $dirname . '.xlsx');
        $reader = IOFactory::createReader($file_type);
        $spreadsheet = $reader->load('downloads/' . $dirname . '/' . $dirname . '.xlsx');
        return $spreadsheet->getActiveSheet()->toArray();
    }
}

//try {
//    Core::edit(Core::chatId(), Core::messageId()-2,'salom qale ishla ' . Core::userName());
//} catch (TelegramSDKException $e) {
//    echo $e->getMessage();
//}