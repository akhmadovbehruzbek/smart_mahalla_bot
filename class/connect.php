<?php

use mysql_xdevapi\Exception;
use RedBeanPHP\R;

try {
    R::setup("mysql:host=" . HOST . ";dbname=" . DB_NAME,
        USER, PASS);
} catch (Exception $e) {
    echo $e->getMessage();
}