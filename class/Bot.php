<?php

namespace class\Bot;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once 'Core.php';
require_once 'Db.php';
require_once 'BotKeyboard.php';

use BotKeyboard\BotKeyboard;
use class\Core\Core;
use class\Db\Db;
use Illuminate\Support\Traits\EnumeratesValues;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use RedBeanPHP\RedException\SQL;
use Telegram\Bot\Exceptions\TelegramSDKException;

class Bot
{
    /** Start Class */

    public function db(): Db
    {
        return new Db();
    }

    /** find user
     * @throws TelegramSDKException
     */
    public function findUser(): bool
    {
        return $this->db()->findUser(Core::chatId()) === null;
    }

    /** add New user
     * @throws TelegramSDKException|SQL
     */
    public function addUser(): int|string
    {
        return $this->db()->addUser(Core::chatId(), $this->page('home'), Core::userName());
    }

    /** Text
     * @throws TelegramSDKException
     */
    public function text(): string
    {
        return Core::text();
    }

    /**
     * @throws TelegramSDKException
     */
    public function getPage()
    {
        if (empty($this->db()->getPage(Core::chatId()))) {
            $page = 'home';
        } else {
            $page = $this->db()->getPage(Core::chatId())['page'];
        }
        return $page;
    }

    /**
     * @throws TelegramSDKException
     */
    public function getUpdates(): array
    {
        return Core::Api()->getUpdates();
    }

    /************ Client start   **********
     * @throws TelegramSDKException
     */
    public function showHomeClient(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'home') {
            if ($this->db()->setPage('home', Core::chatId())) {
                $message = "Assalomu alaykum hurmatli " . Core::userName() . " !  Yoshlar ishlari agentligi Surxondaryo viloyati boshqarmasi Mahalladagi yoshlar yetakchilari bilan ishlash bo'limi botiga xush kelibsiz!";
                return Core::send(
                    Core::chatId(),
                    $message,
                    BotKeyboard::homeKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return Core::send(
            Core::chatId(),
            'Siz bosh sahifadasiz',
            BotKeyboard::homeKeyboard()
        );
    }

    /********** Client Errors **********
     * @throws TelegramSDKException
     */
    public function thisPage($page): \Telegram\Bot\Objects\Message
    {
        $message = $page === 'home' ? 'Siz Bosh sahifadasiz.' : 'Siz Bosh ' . $page . ' sahifasidasiz';
        return Core::send(
            Core::chatId(),
            $message
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function showTumanClient(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'tumanlar') {
            if ($this->db()->setPage('tumanlar', Core::chatId())) {
                return Core::send(
                    Core::chatId(),
                    "Tuman (shahar) nomini tanlang👇",
                    $this->tumanKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisIsAdmin();
    }

    /**
     * @throws TelegramSDKException
     */
    public function showAgentAndMahalla(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'agent_and_mahalla') {
            if ($this->db()->setPage('agent_and_mahalla', Core::chatId())) {
                return Core::send(
                    Core::chatId(),
                    Core::text(),
                    $this->agentAndMahallaKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisPage('Tumanlar');
    }

    /**
     * @throws TelegramSDKException
     */
    public function showTumanBolimClient(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'tumanBolim') {
            if ($this->db()->setPage('tumanBolim', Core::chatId())) {
                $text = "⛔⛔⛔ Hozirda ushbu tuman bo'limlari mavjud emas.";
                if (count($this->tumanBolimKeyboard()['keyboard']) !== 1) {
                    $text = "Bo'limni tanlang 👇";
                }
                return Core::send(
                    Core::chatId(),
                    $text,
                    $this->tumanBolimKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisPage('Tumanlar Bo\'limlari');
    }

    public function showMahallaClient(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'mahalla') {
            if ($this->db()->setPage('mahalla', Core::chatId())) {
                $text = '⛔⛔⛔ Hozirda ushbu tuman mahallari mavjud emas.';
                if (count($this->mahallaKeyboard()['keyboard']) !== 1) {
                    $text = 'Mahalla tanlang.👇';
                }
                return Core::send(
                    Core::chatId(),
                    $text,
                    $this->mahallaKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisPage('Tumanlar Bo\'limlari');
    }

    /**
     * @throws TelegramSDKException
     */
    public function showInfoMahallaClient(): \Telegram\Bot\Objects\Message
    {
        $text = '⛔⛔⛔ Hozirda ushbu mahalla ma\'lumotlari mavjud emas.';
        if (count($this->getMahalla()) !== 0) {
            $text = "<b>🏛Mahalla nomi: </b>" . $this->getMahalla()['name'] . "\n";
            $text .= "<b>👤Mahalla yetakchisi: </b>" . $this->getMahalla()['full_name'] . "\n";
            $text .= "<b>☎️Telefon raqami: </b>" . $this->getMahalla()['tel'] . "\n";
        }
        return Core::send(
            Core::chatId(),
            $text,
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function showInfoTumanBolimClient(): \Telegram\Bot\Objects\Message
    {
        $text = '⛔⛔⛔ Hozirda ushbu Tuman bo\'limi ma\'lumotlari mavjud emas.';
        if (count($this->getAgentlik()) !== 0) {
            $text = "<b>🏛Bo'lim nomi: </b>" . $this->getAgentlik()['name'] . "\n";
            $text .= "<b>👤Bo'lim masuli: </b>" . $this->getAgentlik()['full_name'] . "\n";
            $text .= "<b>🔰Lavozimi: </b>" . $this->getAgentlik()['position'] . "\n";
            $text .= "<b>☎️Telefon raqami: </b>" . $this->getAgentlik()['tel'] . "\n";
        }

        define('BIRINCHI', 'SALOM');

        return Core::send(
            Core::chatId(),
            $text,
        );
    }




    /**
     * @throws TelegramSDKException
     */
    public function getMahalla(): ?array
    {
        return $this->db()->getMahalla($this->text());
    }

    /**
     * @throws TelegramSDKException
     */
    public function getAgentlik(): ?array
    {
        return $this->db()->getAgentlik($this->text());
    }

    /**
     * @throws TelegramSDKException
     */
    public function getTumanId(): ?array
    {
        return $this->db()->getTumaniId(Core::text());
    }

    public function saveTumanId($tuman_id): array|int|null
    {
        return $this->db()->saveTumanId($tuman_id, Core::chatId());
    }

    /****** Keyboards client *****/
    public function tumanKeyboard(): EnumeratesValues|\Telegram\Bot\Keyboard\Keyboard
    {
        $tuman = $this->db()->getTumanAll();
        $tuman_count = $this->db()->count('tuman');
        return BotKeyboard::keyboard($tuman, $tuman_count);
    }

    /**
     */
    public function agentAndMahallaKeyboard(): EnumeratesValues|\Telegram\Bot\Keyboard\Keyboard
    {
//        $count_agentlik = $this->db()->count_mahalla("tumanbolim", $this->db()->getTumanIdByUser(Core::chatId())['tuman_id']);
//        $count_mahalla = $this->db()->count_mahalla("mahalla", $this->db()->getTumanIdByUser(Core::chatId())['tuman_id']);
        return BotKeyboard::agentMahallaKeyboard();
    }

    /**
     * @throws TelegramSDKException
     */
    public function tumanBolimKeyboard(): EnumeratesValues|\Telegram\Bot\Keyboard\Keyboard
    {
        $agentlik = $this->db()->getAgentlikAll($this->getTumanIdByUser()['tuman_id']);
        $agentlik_count = $this->db()->count_mahalla('tumanbolim', $this->getTumanIdByUser()['tuman_id']);

        return BotKeyboard::keyboard($agentlik, $agentlik_count);
    }


    /**
     * @throws TelegramSDKException
     */
    public function mahallaKeyboard(): EnumeratesValues|\Telegram\Bot\Keyboard\Keyboard
    {
        $mahalla = $this->db()->getMahallaAll($this->getTumanIdByUser()['tuman_id']);
        $mahalla_count = $this->db()->count_mahalla('mahalla', $this->getTumanIdByUser()['tuman_id']);

        return BotKeyboard::keyboard($mahalla, $mahalla_count);
    }

    /**
     * @throws TelegramSDKException
     */
    public function getTumanIdByUser(): ?array
    {
        return $this->db()->getTumanIdByUser(Core::chatId());
    }


    /************ Client end   ***********/


    /********  Admin start  ********
     *
     * showHomeAdmin funksiyasi admin kirish qismi
     *
     * @throws TelegramSDKException
     */
    public function showHomeAdmin(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'adminHome') {
            if ($this->db()->setPage('adminHome', Core::chatId())) {
                $message = "Assalomu alaykum " . Core::userName() . " siz " . BOT_NAME . "botining Admin sahifasidasiz. \n";
                $message .= "User sahifasiga o'tish uchun /start buyrug'ini yuboring.\n";
//                $message .= "Bot bilan ishlashni o'rganish uchun /help buyrug'ini yuboring.\n";
                return Core::send(
                    Core::chatId(),
                    $message,
                    BotKeyboard::homeAdminKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisIsAdmin();
    }

    /***************** Tuman page start *******************/
    /**
     * @throws TelegramSDKException
     */
    public function sendTuman(): \Telegram\Bot\Objects\Message
    {
        $tuman = $this->db()->getTumanAll();
        return Core::send(
            Core::chatId(),
            json_encode($tuman, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT),
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function sendTumanBolimAll(): \Telegram\Bot\Objects\Message
    {
        return Core::sendDocument(
            Core::chatId(),
            DOCUMENT_PATH . 'tumanbolim/tumanbolim.xlsx',

        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function sendMahallaAll(): \Telegram\Bot\Objects\Message
    {
//        $mahalla = $this->db()->getMahallaAllAdmin();
//        return Core::send(
//            Core::chatId(),
//            json_encode($mahalla, JSON_PRETTY_PRINT),
//        );

        return Core::sendDocument(
            Core::chatId(),
            DOCUMENT_PATH . 'mahalla/mahalla.xlsx',

        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function showTuman(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'adminTuman') {
            if ($this->db()->setPage('adminTuman', Core::chatId())) {
                return Core::send(
                    Core::chatId(),
                    "Tuman sahifasidasiz",
                    BotKeyboard::adminTumanKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisIsAdmin();
    }

    /**
     * @throws SQL
     * @throws Exception
     */
    public function updateTuman(): int|string
    {
        $data = Core::getDataArray('tuman');

        try {
            $this->db()->trashTable('tuman');
        } catch (\Exception $e) {
            print_r($e);
        }
//        print_r($data);
//        exit();

        if ($this->db()->trashTable('tuman')) {
            return $this->saveTuman($data);
        }
        return false;

    }

    /**
     * @throws SQL
     */
    public function saveTuman($data): int|string
    {
        return $this->db()->addNewTuman($data);
    }

    /**
     * @throws TelegramSDKException
     */
    public function showAddTuman(): \Telegram\Bot\Objects\Message
    {

        if ($this->db()->setPage('adminUpdateTuman', Core::chatId())) {

//            return Core::sendPhoto(
//                Core::chatId(),
//                IMAGE_PATH . "mahalla.png",
//                'Tuman tauboring',
//                BotKeyboard::backAdminKeyboard()
//            );
            return Core::send(
                Core::chatId(),
                '❗Ushbu bo\'lim ishlab chiqilmoqda',
                BotKeyboard::backAdminKeyboard()
            );
        }
        return $this->dataSaveError();
    }

    /*********************** Mahalla page start    ***********************
     * @throws TelegramSDKException
     */
    public function showMahalla(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'adminMahalla') {
            if ($this->db()->setPage('adminMahalla', Core::chatId())) {
                return Core::send(
                    Core::chatId(),
                    "Mahalla sahifasidsasiz",
                    BotKeyboard::adminMahallaKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisIsAdmin();
    }

    /**
     * @throws TelegramSDKException
     */
    public function showAddMahalla(): \Telegram\Bot\Objects\Message
    {
        if ($this->db()->setPage('adminUpdateMahalla', Core::chatId())) {
            return Core::sendPhoto(
                Core::chatId(),
                IMAGE_PATH . "mahalla.png",
                'Mahalla ma\'lumotlarini quyidagi rasmda ko\'rsatilgan tartibda .xlsx formatida yuboring.',
                BotKeyboard::backAdminKeyboard()
            );
        }
        return $this->dataSaveError();
    }

    /**
     * @throws SQL
     * @throws Exception
     */
    public function updateMahalla(): int|string
    {
        $data = Core::getDataArray('mahalla');
        $data_error = [];
        foreach ($data as $val) {
            if (!is_int($val[3])) {
                $data_error[] = 'Raqam kiritish kerak';
            }
        }

        if (empty($data_error)) {
            if ($this->db()->trashTable('mahalla')) {
                return $this->saveMahalla($data);
            }
        }

        return false;
    }

    /**
     * @throws SQL
     */
    public function saveMahalla($data): int|string
    {
        return $this->db()->addNewMahalla($data);
    }

    /************************************  Tuman bolimlari page   ***********************************
     * @throws TelegramSDKException
     */
    public function showTumanBolim(): \Telegram\Bot\Objects\Message
    {
        if ($this->getPage() !== 'adminTumanBolim') {
            if ($this->db()->setPage('adminTumanBolim', Core::chatId())) {
                return Core::send(
                    Core::chatId(),
                    "Tuman bo'limlari sahifasidasiz",
                    BotKeyboard::adminTumanBolimKeyboard()
                );
            }
            return $this->dataSaveError();
        }
        return $this->thisIsAdmin();
    }

    /**
     * @throws TelegramSDKException
     */
    public function showAddTumanBolim(): \Telegram\Bot\Objects\Message
    {
        if ($this->db()->setPage('adminUpdateTumanBolim', Core::chatId())) {
            return Core::sendPhoto(
                Core::chatId(),
                IMAGE_PATH . "tumanbolim.png",
                "Tuman bo'limlari ma'lumotlarini quyidagi rasmda ko'rsatilgan tartibda .xlsx formatida yuboring.",
                BotKeyboard::backAdminKeyboard()
            );
        }
        return $this->dataSaveError();
    }

    /**
     * @throws SQL
     * @throws Exception
     */
    public function updateTumanBolim(): int|string
    {

        $data = Core::getDataArray('tumanbolim');
        $data_error = [];
//        print_r($data);
//        exit();

        foreach ($data as $val) {
            if (!is_int($val[4])) {
                $data_error[] = 'Raqam kiritish kerak';
            }
        }
        if (empty($data_error)) {
            if ($this->db()->trashTable('tumanbolim')) {
                return $this->saveTumanBolim($data);
            }
        }
        return false;
    }

    /**
     * @param $data
     * @return int|string
     */
    public function saveTumanBolim($data): int|string
    {
        return $this->db()->addNewTumanBolim($data);
    }


    /**
     * @throws TelegramSDKException
     */
    public function setFile($name): bool|int
    {
        return Core::setFile($name);
    }


    /**
     * @throws TelegramSDKException
     */
    public function isAdmin(): bool
    {
        return Core::chatId() == ADMIN_CHAT_ID_1 || Core::chatId() == ADMIN_CHAT_ID_2;
    }


    /** Admin end */

    /***************** Errors *********
     * @throws TelegramSDKException
     */
    public function dataSaveError(): \Telegram\Bot\Objects\Message
    {
        return Core::send(
            Core::chatId(),
            'Xatolik yuz berdi',
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function dataError(): \Telegram\Bot\Objects\Message
    {
        return Core::send(
            Core::chatId(),
            "❗Excel fayldagi ma'lumotda xatolik bor. Tuman id si xato kiritilgan bolishi mumkin. Iltimos tekshirib qaytadan urinib ko'ring."
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function notFound(): \Telegram\Bot\Objects\Message
    {
        return Core::send(
            Core::chatId(),
            'Not Found'
        );
    }

    /**
     * @throws TelegramSDKException
     */
    public function thisIsAdmin(): \Telegram\Bot\Objects\Message
    {
        return Core::send(
            Core::chatId(),
            'Admin sahifasidasiz'
        );
    }

    public function page($page): string
    {
        return $page !== "" ? $page : 'tuman';
    }

    /**********   success true
     * @throws TelegramSDKException
     */
    public function successTrue(): \Telegram\Bot\Objects\Message
    {
        return Core::send(
            Core::chatId(),
            'Muvaffaqiyatli bajarildi. 👍'
        );
    }


    /** End Class */
}