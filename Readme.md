Assalomu alaykum ``Smart_mahalla_bot_surxondayo`` ga xush kelibsiz.

``` Smart mahalla bot ni o'rnatish ```

1. composer update
2. database `database` papkasi ichida ushbu db ni o'rnatiladi.
3. `.env` fayli ga
   BOT_TOKEN=Your Bot token
   ADMIN_CHAT_ID=admin chat id
   WEBHOOK_URL=webhook url (https://domain.com/file.php)
   HOST=hostname
   USER=user name
   PASS= host password
   DB_NAME= database name

```Admin qismi```
-
1. `/admin` kamandasini kiritish orqali admin qismiga kirish mumkin
2. `Tuman` tanlanganda tumanlar ro'yxati va id si json shaklida taqdim etiladi
3. `Agentliklar` yuklash uchun excel fayl yaratiladi

| agentlik nomi 	| agent to'liq ismi 	| agent lavozimi 	| agent tel raqami 	| tuman id si (tuman bo'limidagi id kiritiladi - raqam) 	|
|---------------	|-------------------	|----------------	|------------------	|-------------------------------------------------------	|
|               	|                   	|                	|                  	|                                                       	|

4. `Mahalla` yuklash uchun excel fayl yaratiladi

| mahalla nomi 	| mahalla xodimi to'liq ismi  	| mahalla xodimi tel raqami 	| tuman id si (tuman bo'limidagi id kiritiladi - raqam) 	|
|--------------	|-----------------------------	|---------------------------	|-------------------------------------------------------	|
|              	|                             	|                           	|                                                       	|


