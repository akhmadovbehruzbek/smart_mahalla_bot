<?php

use class\Core\Core;
//
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
require_once 'class/Bot.php';

$bot = new \class\Bot\Bot();

//file_put_contents('log.json', \class\Core\Core::Api()->getWebhookUpdate());
//file_put_contents('log.xlsx', \class\Core\Core::downloadFile());


try {
    if ($bot->findUser()) {
        $bot->addUser();
    }
} catch (\RedBeanPHP\RedException\SQL|\Telegram\Bot\Exceptions\TelegramSDKException $e) {
    file_put_contents('error.json', json_encode($bot->getUpdates(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
}

/*********  Client section start  ************/
try {
    if ($bot->text() === '/start') {
        $bot->showHomeClient();
    } else {
        switch ($bot->getPage()) {
            case 'home' :
                if ($bot->text() === "Hududlar🏛") {
                    $bot->showTumanClient();
                }
                break;
            case 'tumanlar' :
                if ($bot->text() === '🔙Orqaga') {
                    $bot->showHomeClient();
                } elseif ($bot->getTumanId() !== []) {
                    $tuman_id = $bot->getTumanId();
                    $bot->showAgentAndMahalla();
                    $bot->saveTumanId($tuman_id['id']);
                }
                break;
            case 'agent_and_mahalla' :
                if ($bot->text() === "Bo'lim xodimlari") {
                    $bot->showTumanBolimClient();
                } elseif ($bot->text() === "Mahalla yetakchilari") {
                    $bot->showMahallaClient();
                } elseif ($bot->text() === '🔙Orqaga') {
                    $bot->showTumanClient();
                }
                break;

            case "tumanBolim":
                if ($bot->text() === '🔙Orqaga') {
                    $bot->showAgentAndMahalla();
                } else {
                    $bot->showInfoTumanBolimClient();
                }
                break;

            case "mahalla":
                if ($bot->text() === '🔙Orqaga') {
                    $bot->showAgentAndMahalla();
                } else {
                    $bot->showInfoMahallaClient();
                }
                break;

        }
    }
} catch (\Telegram\Bot\Exceptions\TelegramSDKException $e) {
}

/*********  Client section end  ************/

/********* Admin start *******/
try {
    if ($bot->isAdmin()) {
        if ($bot->text() === '/admin') {
            $bot->showHomeAdmin();
        } else {

            switch ($bot->getPage()) {
                /*** Admin home page start  */
                case 'adminHome' :
                    if ($bot->text() === 'Tuman') {
                        $bot->showTuman();
                    } elseif ($bot->text() === 'Mahalla') {
                        $bot->showMahalla();
                    } elseif ($bot->text() === "Tuman Bo'limlari") {
                        $bot->showTumanBolim();
                    } else {
                        $bot->notFound();
                    }
                    break;

                /*****  Admin Tuman page start  **/
                case 'adminTuman' :
                    if ($bot->text() === 'Barcha tumanlar') {
                        $bot->sendTuman();
                    } elseif ($bot->text() === '🔙Orqaga') {
                        $bot->showHomeAdmin();
                    } elseif ($bot->text() === 'Tumanlarni yangilash') {

                        $bot->showAddTuman();

                    } else {
                        $bot->notFound();
                    }
                    break;

                case 'adminUpdateTuman' :

                    if ($bot->text() === "🔙Orqaga") {
                        $bot->showTuman();

                    }
//                    elseif (isset(Core::result('message')['document'])) {
//                        if ($bot->setFile('tuman')) {
//                            try {
//                                if ($bot->updateTuman()) {
//                                    return $bot->successTrue();
//                                }
//                                return $bot->dataError();
//                            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception|\RedBeanPHP\RedException\SQL $e) {
//                                file_put_contents('tuman_error.json', json_encode($bot->getUpdates(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
//                            }
//                        }
//
//                    }
                    else {
                        $bot->dataSaveError();
                    }
                    break;

                /************ Admin Mahalla page start ******************/
                case 'adminMahalla' :
                    if ($bot->text() === 'Barcha mahallalar') {
                        $bot->sendMahallaAll();
                    } elseif ($bot->text() === '🔙Orqaga') {
                        $bot->showHomeAdmin();
                    } elseif ($bot->text() === 'Mahallalarni yangilash') {
                        $bot->showAddMahalla();
                    } else {
                        $bot->notFound();
                    }
                    break;
                case 'adminUpdateMahalla' :
                    if ($bot->text() === "🔙Orqaga") {
                        $bot->showMahalla();
                    } elseif (isset(Core::result('message')['document'])) {
                        if ($bot->setFile('mahalla')) {
                            try {
                                if ($bot->updateMahalla()) {
                                    return $bot->successTrue();
                                }
                                return $bot->dataError();
                            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception|\RedBeanPHP\RedException\SQL $e) {
                                file_put_contents('mahalla_error.json', json_encode($bot->getUpdates(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
                            }
                        }
                    } else {
                        $bot->notFound();
                    }
                    break;

                /****************** Admin Tuman bo'limlari page start   ********************/

                case 'adminTumanBolim' :
                    if ($bot->text() === "Barcha tuman bo'limlari") {
                        $bot->sendTumanBolimAll();
                    } elseif ($bot->text() === '🔙Orqaga') {
                        $bot->showHomeAdmin();
                    } elseif ($bot->text() === "Tuman bo'limlarini yangilash") {
                        $bot->showAddTumanBolim();
                    } else {
                        $bot->notFound();
                    }
                    break;

                case 'adminUpdateTumanBolim' :
                    if ($bot->text() === "🔙Orqaga") {
                        $bot->showTumanBolim();
                    } elseif (isset(Core::result('message')['document'])) {
                        if ($bot->setFile('tumanbolim')) {
                            try {
                                if ($bot->updateTumanBolim()) {
                                    return $bot->successTrue();
                                }
                                return $bot->dataError();
                            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception|\RedBeanPHP\RedException\SQL $e) {
                                file_put_contents('tumanBolim_error.json', $e->getMessage());
                            }
                        }
                    } else {
                        $bot->notFound();
                    }
                    break;

            }
        }

    }
} catch (JsonException|\Telegram\Bot\Exceptions\TelegramSDKException $e) {
    file_put_contents('error_log.txt', $e->getMessage(), FILE_APPEND | LOCK_EX);
}
/********* Admin end *******/


